from conans import ConanFile, CMake, tools
import os
import glob

class Blend2dConan(ConanFile):
    name = "blend2d"
    license = "zlib"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-blend2d/"
    homepage = "https://github.com/blend2d/blend2d"
    description = "2D Vector Graphics Engine Powered by a JIT Compiler"
    topics = ("2d-graphics", "rasterization", "asmjit", "jit")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"

    def source(self):
        sources = self.conan_data["sources"][self.version]

        tools.get(**{k : sources[k] for k in ['url', 'sha256']})
        extracted_dir = glob.glob('blend2d*/')[0]
        os.rename(extracted_dir, "blend2d")

        if "asmjit" in sources.keys():
            tools.get(**sources["asmjit"])
            extracted_dir = glob.glob('asmjit*/')[0]
            os.rename(extracted_dir, "asmjit")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BLEND2D_TEST"] = False
        cmake.definitions["BLEND2D_EMBED"] = False
        cmake.definitions["BLEND2D_STATIC"] = not self.options.shared
        cmake.configure(source_folder="blend2d")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="blend2d/src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["blend2d"]
        if self.settings.os == "Linux":
            self.cpp_info.libs.extend(["pthread", "rt"])
